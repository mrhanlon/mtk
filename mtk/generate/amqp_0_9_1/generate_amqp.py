#!/usr/bin/env python

###############################################################################
#   Copyright 2012 Warren Smith                                               #
#                                                                             #
#   Licensed under the Apache License, Version 2.0 (the "License");           #
#   you may not use this file except in compliance with the License.          #
#   You may obtain a copy of the License at                                   #
#                                                                             #
#       http://www.apache.org/licenses/LICENSE-2.0                            #
#                                                                             #
#   Unless required by applicable law or agreed to in writing, software       #
#   distributed under the License is distributed on an "AS IS" BASIS,         #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
#   See the License for the specific language governing permissions and       #
#   limitations under the License.                                            #
###############################################################################

import copy
import math
import optparse
import os
import re
import sys

from mako.template import Template

from mtk.generate.amqp.parse import Parser

#######################################################################################################################

def constantName(name):
    return re.sub(r"-","_",name).upper()

def variableName(name):
    return re.sub(r"-","_",name)

#######################################################################################################################

constant_template = """

PROTOCOL_MAJOR = ${parser.major}
PROTOCOL_MINOR = ${parser.minor}
PROTOCOL_REVISION = ${parser.revision}

PORT = ${parser.port}

% for constant in parser.constants:
  % for doc_line in constant.doc:
# ${doc_line.lstrip()}
  % endfor
${constant.python_name} = ${constant.value}

% endfor

"""

def writeConstants(parser, src_dir):
    f = open(src_dir+"/constants.py","w")
    f.write(generateConstants(parser))
    f.close()

def generateConstants(parser):
    tmpl = Template(constant_template)
    return tmpl.render(parser=parser)

#######################################################################################################################

init_template = """

% for doc_line in cls.doc:
# ${doc_line}
% endfor

% for method in cls.methods:
from ${method.python_name} import ${method.python_name}
% endfor

"""

method_template = """

import struct

from ${package}.frame import Method
from ${package}.table import FieldTable

% for rmethod in method.response_methods:
from ${rmethod.python_name} import ${rmethod.python_name}
% endfor

class ${method.python_name}(Method):
    \"\"\"
% for doc_line in method.doc:
${doc_line}
% endfor
    \"\"\"

% if method.synchronous:
    SYNCHRONOUS = True
% else:
    SYNCHRONOUS = False
% endif

    RESPONSE = []
% for rmethod in method.response_methods:
    RESPONSE.append(${rmethod.python_name})
% endfor

    def __init__(self, channel=0):
        Method.__init__(self,${cls.index},${method.index},channel)
% for field in method.fields:

    % for doc_line in field.doc:
        # ${doc_line.lstrip()}
    % endfor
    % if field.getType() == "shortstr":
        self.${field.python_name} = ""    # ${field.getType()}
    % elif field.getType() == "longstr":
        self.${field.python_name} = ""    # ${field.getType()}
    % elif field.getType() == "table":
        self.${field.python_name} = FieldTable()    # ${field.getType()}
    % else:
        self.${field.python_name} = 0    # ${field.getType()}
    % endif
% endfor

% if len(method.fields) == 0:
        pass
% endif

    def __str__(self, indent=""):
        mstr = indent + "${cls.python_name}.${method.python_name}:\\n"
% for field in method.fields:
    % if field.getType() == "table":
        mstr += indent + "    ${field.python_name}:\\n"
        mstr += self.${field.python_name}.__str__(indent+"        ")
    % else:
        mstr += indent + "    ${field.python_name}: " + str(self.${field.python_name}) + "\\n"
    % endif
% endfor
        return mstr


    def payloadSize(self):
        l = 4    # class-id (short), method-id (short)
% for field in method.fields:

        # ${field.python_name}
    % if field.getType() == "bit":
        % if field.last_bit:
        l += 1    # one to eight bits
        % endif
    % elif field.getType() == "octet":
        l += 1    # octet
    % elif field.getType() == "short":
        l += 2    # 16-bit integer
    % elif field.getType() == "long":
        l += 4    # 32-bit integer
    % elif field.getType() == "longlong":
        l += 8    # 64-bit integer
    % elif field.getType() == "shortstr":
        l += 1 + len(self.${field.python_name})    # short string
    % elif field.getType() == "longstr":
        l += 4 + len(self.${field.python_name})    # long string
    % elif field.getType() == "table":
        l += self.${field.python_name}.length()
    % endif
% endfor

        return l

    def packPayload(self, buffer):
        struct.pack_into("!HH",buffer,7,self._class_id,self._method_id)
        offset = 7 + 4
% for field in method.fields:

    % if field.getType() == "bit":
        % if field.first_bit:
        byte = 0
        % endif
        if self.${field.python_name} > 0:
            byte = byte | 1 << ${field.bit_index}
        % if field.last_bit:
        struct.pack_into("!B",buffer,offset,byte)
        offset += 1
        % endif
    % elif field.getType() == "octet":
        struct.pack_into("!B",buffer,offset,self.${field.python_name})
        offset += 1
    % elif field.getType() == "short":
        struct.pack_into("!H",buffer,offset,self.${field.python_name})
        offset += 2
    % elif field.getType() == "long":
        struct.pack_into("!I",buffer,offset,self.${field.python_name})
        offset += 4
    % elif field.getType() == "longlong":
        struct.pack_into("!Q",buffer,offset,self.${field.python_name})
        offset += 8
    % elif field.getType() == "shortstr":
        struct.pack_into("!B",buffer,offset,len(self.${field.python_name}))
        offset += 1
        struct.pack_into("!%ds" % len(self.${field.python_name}),buffer,offset,self.${field.python_name})
        offset += len(self.${field.python_name})
    % elif field.getType() == "longstr":
        struct.pack_into("!I",buffer,offset,len(self.${field.python_name}))
        offset += 4
        struct.pack_into("!%ds" % len(self.${field.python_name}),buffer,offset,self.${field.python_name})
        offset += len(self.${field.python_name})
    % elif field.getType() == "timestamp":
        struct.pack_into("!Q",buffer,offset,self.${field.python_name})
        offset += 8
    % elif field.getType() == "table":
        offset = self.${field.python_name}.pack(buffer,offset)
    % else:
        raise Exception("unknown type: %{field.getType()}")          
    % endif
% endfor

        return offset

    @staticmethod
    def unpackPayload(channel, buffer):
        method = ${method.python_name}(channel)

        offset = 4    # class_id and method_id are already known
% for field in method.fields:

    % if field.getType() == "bit":
        (byte,) = struct.unpack_from("!B",buffer,offset)
        if byte & 1 << ${field.bit_index} > 0:
            method.${field.python_name} = 1
        else:
            method.${field.python_name} = 0
        % if field.last_bit:
        offset += 1
        % endif
    % elif field.getType() == "octet":
        (method.${field.python_name},) = struct.unpack_from("!B",buffer,offset)
        offset += 1
    % elif field.getType() == "short":
        (method.${field.python_name},) = struct.unpack_from("!H",buffer,offset)
        offset += 2
    % elif field.getType() == "long":
        (method.${field.python_name},) = struct.unpack_from("!I",buffer,offset)
        offset += 4
    % elif field.getType() == "longlong":
        (method.${field.python_name},) = struct.unpack_from("!Q",buffer,offset)
        offset += 8
    % elif field.getType() == "shortstr":
        (strlen,) = struct.unpack_from("!B",buffer,offset)
        offset += 1
        (method.${field.python_name},) = struct.unpack_from("!%ds" % strlen,buffer,offset)
        offset += strlen
    % elif field.getType() == "longstr":
        (strlen,) = struct.unpack_from("!I",buffer,offset)
        offset += 4
        (method.${field.python_name},) = struct.unpack_from("!%ds" % strlen,buffer,offset)
        offset += strlen
    % elif field.getType() == "timestamp":
        (method.${field.python_name},) = struct.unpack_from("!Q",buffer,offset)
        offset += 8
    % elif field.getType() == "table":
        method.${field.python_name} = FieldTable()
        offset = method.${field.python_name}.unpack(buffer,offset)
    % else:
        raise Exception("unknown type: %{field.getType()}")          
    % endif
% endfor

        return method
"""

def writeClasses(parser, src_dir):
    for cls in parser.classes:
        try:
            os.makedirs(src_dir+"/"+cls.python_name)
        except OSError:
            pass  # the directory probably already exists

        f = open(src_dir+"/"+cls.python_name+"/__init__.py","w")
        tmpl = Template(init_template)
        f.write(tmpl.render(cls=cls))
        f.close()

        for method in cls.methods:
            f = open(src_dir+"/"+cls.python_name+"/"+method.python_name+".py","w")
            tmpl = Template(method_template)
            f.write(tmpl.render(cls=cls,
                                method=method,
                                package="mtk.amqp_"+str(parser.major)+"_"+str(parser.minor)+"_"+str(parser.revision)))
            f.close()

    f = open(src_dir+"/methods.py","w")
    for cls in parser.classes:
        f.write("from mtk.amqp_"+str(parser.major)+"_"+str(parser.minor)+"_"+str(parser.revision)+".gen"+
                " import "+cls.python_name+"\n")
    f.write("\n")
    f.write("methods = {}\n")
    for cls in parser.classes:
        for method in cls.methods:
            f.write("methods[("+str(cls.index)+","+str(method.index)+")] = "+
                    cls.python_name+"."+method.python_name+"\n")
    f.close()

#######################################################################################################################

if __name__ == "__main__":

    opt = optparse.OptionParser(usage="usage: %prog [options] <xml specification file>")
    opt.add_option("-g","--gendir",action="store",type="string",dest="src_dir",
                   help="path to the (non-default) directory that will contain the generated files")
    (options,args) = opt.parse_args()

    if len(args) != 1:
        print("Error: specify exactly one xml specification file")
        opt.print_help()
        sys.exit(1)

    parser = Parser(args[0])

    src_dir = options.src_dir
    if src_dir == None:
        # __file__ is /path/to/mtk/sbin/generate_amqp.py
        (path,executable) = os.path.split(os.path.abspath(__file__))
        (path,sbin) = os.path.split(path)
        src_dir = os.path.join(path,"lib","mtk",
                               "amqp_"+str(parser.major)+"_"+str(parser.minor)+"_"+str(parser.revision),"gen")

    try:
        os.makedirs(src_dir)
    except OSError:
        pass  # the directory probably already exists

    f = open(src_dir+"/__init__.py","w")
    f.close()

    writeConstants(parser,src_dir)

    writeClasses(parser, src_dir)
